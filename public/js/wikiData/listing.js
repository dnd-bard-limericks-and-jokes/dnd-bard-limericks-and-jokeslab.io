export default class listing{
  constructor(title, alias, seeAlso, content, url, external, uses, altListings){
    this.seeAlso = seeAlso || [];
    this.title = title;
    this.url = url || title;
    this.alias = alias || [];
    this.external = external || [];
    this.altListings = altListings || [];
    this.uses = uses || false;
    this.rawContent = content;
  }
  
  findListing(regxpSearch){
    // this is so it's case insensitive.
    if(this.url.search(regxpSearch) > -1){
      return true;
    } else if(this.title.search(regxpSearch) > -1){
      return true;
    } else {
      for(let i = 0; i < this.alias.length; i++){
        if(this.alias[i].search(regxpSearch) > -1){
          return true;
        }
      }
      return false;
    }
  }
  
  search(text){
    // this is so it's case insensitive.
    let regxp = new RegExp(title, "i");
    if(this.content.search(regxp) > -1){
      return true;
    } else if(this.title.search(regxp) > -1){
      return true;
    } else {
      for(let i = 0; i < this.alias.length; i++){
        if(this.alias[i].search(regxp) > -1)return true;
      }
      return false;
    }
  }
}
