import listing from './listing.js';

export default function loadJson(data){
  let articles = [];
  for(let i = 0; i < data.length; i++){
    let title = data[i].title;
    let alias = data[i].alias || [];
    let seeAlso = data[i].see || [];
    let content = data[i].content;
    let url = data[i].url || false;
    let external = data[i].external || [];
    let uses = data[i].uses || false;
    let altListings = data[i].altListings || [];
    let article = new listing(title, alias, seeAlso, content, url, external, uses, altListings);
    articles.push(article);
  }
  return articles;
}
