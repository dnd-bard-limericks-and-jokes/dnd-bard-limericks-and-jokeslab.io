import listing from "./listing.js";

const title = "Home";
const alias = [];
const seeAlso = [];
const content = "<p>This is a wiki for Bards who are not that good at makeing stuff up, you can search the content for jokes and limrics to use while you are rollplaying.</p><p>Feel free to fork this repo to add your own jokes in, just remember to put in a merge request so others can see your content also!</p>";
let article = new listing(title, alias, seeAlso, content);
export default article;
